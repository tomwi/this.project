<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "teams".
 *
 * @property integer $teamnumber
 * @property string $teamleader
 * @property string $teammembers
 * @property string $note
 */
class Teams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teams';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teamnumber', 'teamleader', 'teammembers', 'note'], 'required'],
            [['teamnumber'], 'integer'],
            [['teamleader', 'teammembers', 'note'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teamnumber' => 'Teamnumber',
            'teamleader' => 'Teamleader',
            'teammembers' => 'Teammembers',
            'note' => 'Note',
        ];
    }
}
