<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal_profile".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $teamleader
 * @property integer $created_at
 * @property integer $created_by
 * @property string $notes
 */
class PersonalProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personal_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'firstname', 'lastname', 'teamleader','attendence'], 'required'],
            [['id', 'created_at', 'created_by', 'attendence'], 'integer'],
            [['firstname', 'lastname', 'teamleader', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'teamleader' => 'Teamleader',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'notes' => 'Notes',
            'attendence'=> 'Attendence',
        ];
    }
}
