<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property string $skills
 * @property string $education
 * @property string $field
 * @property string $languages
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'skills', 'education', 'field', 'languages'], 'required'],
            [['id'], 'integer'],
            [['skills', 'education', 'field', 'languages'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'skills' => 'Skills',
            'education' => 'Education',
            'field' => 'Field',
            'languages' => 'Languages',
        ];
    }
}
