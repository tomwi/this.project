<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cash".
 *
 * @property integer $ID
 * @property string $date
 * @property string $whobaught
 * @property string $place
 * @property string $used
 * @property integer $invoicenum
 * @property string $returned
 */
class Cash extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'whobaught', 'place', 'used', 'invoicenum', 'returned', 'amount'], 'required'],
            [['ID', 'invoicenum', 'amount'], 'integer'],
            [['date'], 'safe'],
            [['whobaught', 'place', 'used', 'returned'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'date' => 'Date',
            'whobaught' => 'Whobaught',
            'place' => 'Place',
            'used' => 'Used',
            'invoicenum' => 'Invoicenum',
            'returned' => 'Returned',
            'amount' => 'Amount',
        ];
    }
}
