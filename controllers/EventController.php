<?php

namespace app\controllers;


use Yii;
use app\models\Event;
use app\models\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use marekpetras\fullcalender;
use yii\behaviors\TimestampBehavior;

/**
 * EventController implements the CRUD actions for event model.
 */
class EventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             TimestampBehavior::className(),

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all event models.
     * @return mixed
     */
        public function actionIndex()
    {

      $events = Event::find()->all();
        
      $tasks=[];  
      foreach ($events AS $eve){
      //Testing
      $event = new \yii2fullcalendar\models\Event();
      $event->id = $eve->id;
      $event->backgroundColor='red';
      $event->title = $eve->title;
      $event->start = $eve->created_date; 
    //  $event->start = date('Y-m-d\Th:m:s\Z',strtotime('6am'));

      
      $tasks[] = $event;
    }
        
        return $this->render('index', ['events' => $tasks,
            ]);
        
    }



    /**
     * Displays a single event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   /* public function actionCreate($date)
    {
        $model = new Event();
        $model->created_date = $date;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,

            ]);
        }
    }**/
public function actionCreate()
{
    $model = new Event();

    if ($model->load(Yii::$app->request->post())) {
        $model->created_date=date('Y-m-d h:m:s');
        $model->save(false);

        return $this->redirect(['view', 'id' => $model->id]);
    } else {
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }
}
    /**
     * Updates an existing event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
