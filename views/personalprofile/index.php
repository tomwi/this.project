<?php
namespace yii\bootstrap;


use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use miloschuman\highcharts\Highcharts;
use kartik\sidenav\SideNav;



/* @var $this yii\web\View */
/* @var $searchModel app\models\personalprofileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personalprofiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personalprofile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Personalprofile', ['create'], ['class' => 'btn btn-success']) ?>
    </p>



             
    <?php 
        $Gridcolumns = [

        'firstname',
        'lastname',
        'teamleader',
        'attendence',
      

        ];

echo ExportMenu::widget([
           'dataProvider' => $dataProvider,
            'columns' => $Gridcolumns

            ]);
            ?>

         



<?php echo Highcharts::widget([

   'options' => [
      'title' => ['text' => ' children attendence'],
      'xAxis' => [
         'categories' => ['attendence', 'activity', 'grade']
      ],
      'yAxis' => [
         'title' => ['text' => 'Fruit eaten']
      ],
      'series' => [
         ['name' => 'Jane', 'data' => [1, 0, 4]],
         ['name' => 'John', 'data' => [5, 7, 3]]
      ]
   ]
]); ?>
<?php
echo Progress::widget([
    'bars' => [
        ['percent' => 30, 'options' => ['class' => 'progress-bar-danger']],
        ['percent' => 30, 'label' => 'test', 'options' => ['class' => 'progress-bar-success']],
        ['percent' => 35, 'options' => ['class' => 'progress-bar-warning']],
    ]
]); ?>


          
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'id',
            'firstname',
            'lastname',
            'teamleader',
            'attendence',


            'created_at',
            // 'created_by',
            // 'notes',

            ['class' => 'yii\grid\ActionColumn'],
        ],

        
    ]); ?>
</div>
